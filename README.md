# Text formatting app
## Zadanie rekrutacyjne numer 2 dla Focus Telecom Polska

Aplikacja stworzona do nakładania filtrów na zadany tekst, posiada interfejs wejściowy w postaci serwera HTTP, na który są wysyłane requesty z danymi mówiącymi o tym:

- jaki tekst trzeba przetworzyć
- jakie filtry będą na tekst nakładane
- na jaki adres URL odsyłać informację o postępie przetwarzania (opcjonalny parametr)


## Uruchom aplikację

1. Zainstaluj  [docker](https://docs.docker.com) oraz [docker-compose](https://docs.docker.com/compose/) 
2. Opcjonalnie: możesz zmienić parametry połączenia z bazą danych w pliku `.env`
3. Uruchom `docker-compose up --build`

## Przetestuj

Przykład requestu:


```json
{
"text": "I ate delicious breakfast",
"filters": ["title", "upper", "lower"],
"progress_url": "http://testsiteurl.pl/test"
}
```

Wynik formatowania:


```json
[
    {
        "id": 0,
        "filter": "title",
        "text": "I Ate Delicious Breakfast"
    },
    {
        "id": 1,
        "filter": "upper",
        "text": "I ATE DELICIOUS BREAKFAST"
    },
    {
        "id": 2,
        "filter": "lower",
        "text": "i ate delicious breakfast"
    }
]
```


Możliwe filtry:

- capitalize (pierwsza litera zamieniona na dużą, reszta na małe)
- lower (wszystkie znaki zamienione na małe)
- title (pierwszy znak każdego wyrazu zamieniony na dużą literę, reszta liter na małe)
- upper (wszystkie znaki zamienione na duże)
- stragne_format (wielkość znaków się naprzemiennie zmienia np. jestem testem -> jEsTeM tEsTeM


po każdym zaaplikowanym filtrze program wysyła żądania HTTP POST z rezultatem przetwarzania na adres podany w danych wejściowych.  

Historia oraz status (InProgress/Done) aktualnie przetwarzanych elementów są dostępne do wglądu pod endpointem `/status/{id}`.

## Wykorzystane technologie:

- Python 3.7
- AsyncIO
- Postgresql
- SQLAlchemy + Alembic
- PyTest





