#!/bin/sh

./wait-for.sh "${POSTGRES_HOST}:${POSTGRES_PORT}" -- \
  echo "${POSTGRES_HOST}:${POSTGRES_PORT} is available" && \
  alembic upgrade head

python app.py
