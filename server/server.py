import typing as t

from aiohttp import web
from aiohttp_validate import validate
from loguru import logger

from server.format import apply_filter
from db import query as q
from db.db_session import get_db_session
from db import schemas as sch


def install(app: web.Application):
    app.router.add_post('/format', format_text)
    app.router.add_get('/status', status)


LOG_EVENT_TYPE = ['TEXT FORMATTING', 'STATUS']


@validate(
    request_schema={
        'type': 'object',
        'description': 'Schema for formatText request',
        'properties': {
            'text': {
                'type': 'string',
                'minLength': 2,
                'maxLength': 2000
            },
            'filters': {
                'type': 'array',
                'items': {
                    'type': 'string',
                    'enum': ['capitalize', 'upper', 'lower', 'title', 'strange_format']
                }
            },
            'progress_url': {
                'type': 'string',
            }
        },
        'required': ['text', 'filters']
    },
)
async def format_text(body: t.Dict, request: web.Request) -> web.Response:
    # aiohttp_validate returns request body after validation
    text = body['text']
    filters = body['filters']
    url = body.get('progress_url', '')

    logger.info(f'Received request to format text "{text}" using filters {filters}')

    with get_db_session() as db_ses:
        obj = q.save_element(text, db_ses)
        obj_id = obj.id

        for f in filters:
            formatted = apply_filter(text, f)
            format_hist = sch.History(
                text_id=obj_id,
                formatted=formatted,
                filter=f,
                queue=filters.index(f),
            )
            q.save_format_history(format_hist, db_ses)

            if url:
                await request.app['session'].post(url, data=formatted)

        q.update_request_status(obj_id, db_ses)

        # add log information to database
        q.add_log(
            m=f'Request to format text "{text}" using filters {filters} was processed.',
            event_type=LOG_EVENT_TYPE[0],
            db=db_ses,
        )
    logger.info(f'Request {obj_id} was processed successfully.')
    return web.json_response(data={
        'id': obj_id,
        'info': f'Request {obj_id} was processed successfully.',
        'text': text,
    })


async def status(request: web.Request) -> web.Response:
    logger.info(f'Received request to show status of the element')
    try:
        element_id = int(request.query['id'])
    except KeyError:
        logger.exception('Element id was not provided.')
        return web.json_response(data='Please provide element id', status=400)

    if element_id:
        with get_db_session() as db_ses:
            element = q.get_element(element_id, db_ses)
            if element is None:
                logger.exception(f'Element with id {element_id} not found')
                return web.json_response(data=f'Element with id {element_id} not found', status=400)
            history = q.get_history(element_id, db_ses)

        # add log information to database
        q.add_log(
            m=f'Request to show element {element_id} status was processed.',
            event_type=LOG_EVENT_TYPE[1],
            db=db_ses,
        )
        return web.json_response(data={
            'original': element.text,
            'status': 'Done' if element.done else 'In Progress',
            'formatting history': [{
                'id': h.queue,
                'filter': h.filter,
                'text': h.formatted,
            } for h in history],
        })
