FROM python:3.8-alpine as base

WORKDIR /text-formatting-app

RUN apk update --no-cache \
&& apk add build-base postgresql-dev libpq --no-cache --virtual .build-deps

RUN pip install --no-cache-dir --upgrade pip

COPY ./requirements.txt ./
RUN pip install --no-cache-dir -r requirements.txt

COPY . .

RUN apk del .build-deps
RUN apk add postgresql-libs libpq --no-cache

EXPOSE 8080

CMD ["./run.sh"]
