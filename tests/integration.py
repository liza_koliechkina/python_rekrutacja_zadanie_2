import os

import requests as r

API_URL = f'http://localhost:{os.environ.get("API_PORT", 8080)}'

TEXT = 'I ate delicious breakfast'


def test_text_formatting() -> None:
    resp = r.post(
        f'{API_URL}/format',
        json={
            'text': TEXT,
            'filters': ['title', 'lower'],
            'progress_url': 'https://webhook.site/18075d03-2049-4cfe-b1bf-b09b838af691',
        },
    )

    assert resp.ok
    assert resp.status_code == 200
    assert resp.json()['text'] == TEXT


def test_text_formatting_fail_bad_request() -> None:
    assert r.post(
        f'{API_URL}/format',
        json={
            'filters': ['title', 'lower'],
        },
    ).status_code == 400

    assert r.post(
        f'{API_URL}/format',
        json={
            'text': TEXT,
            'filters': ['title', 'lower', 'test'],
        },
    ).status_code == 400


def test_formatting_history() -> None:
    filters = ['title', 'upper']
    resp = r.post(
        f'{API_URL}/format',
        json={
            'text': TEXT,
            'filters': filters,
        },
    )

    assert resp.ok
    element_id = resp.json()['id']

    history = r.get(f'{API_URL}/status?id={element_id}')
    assert history.ok
    assert history.status_code == 200

    expected_hist = [{
        'id': filters.index(f),
        'filter': f,
        'text': TEXT.upper() if f == 'upper' else TEXT.title()
    } for f in filters]

    body = history.json()
    assert body['original'] == TEXT
    assert body['status'] == 'Done'
    assert body['formatting history'] == expected_hist


def test_formatting_history_fail_id_not_given() -> None:
    resp = r.get(f'{API_URL}/status')
    assert resp.status_code == 400
    assert resp.json() == 'Please provide element id'

