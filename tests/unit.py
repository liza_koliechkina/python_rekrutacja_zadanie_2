import pytest

from server.format import apply_filter, Filter


@pytest.mark.parametrize(
    'text,filter,expected',
    (
        ('I ate delicious breakfast', 'upper', 'I ATE DELICIOUS BREAKFAST'),
        ('I ate delicious breakfast', 'title', 'I Ate Delicious Breakfast'),
        ('I ATE DELICIOUS BREAKFAST', 'lower', 'i ate delicious breakfast'),
        ('only lower letters', 'capitalize', 'Only lower letters'),
        ('only lower letters', 'strange_format', 'oNlY LoWeR LeTtErS'),
    ),
)
def test_apply_filter(text: str, filter: str, expected: str) -> None:
    result = apply_filter(text, Filter(filter))
    assert result == expected
