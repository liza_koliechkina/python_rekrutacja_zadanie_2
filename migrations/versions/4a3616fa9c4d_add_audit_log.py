"""Add audit log

Revision ID: 4a3616fa9c4d
Revises: b1d88bac2606
Create Date: 2021-03-17 21:50:10.864282

"""
import datetime

from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '4a3616fa9c4d'
down_revision = 'b1d88bac2606'
branch_labels = None
depends_on = None


def upgrade():
    op.create_table(
        'audit_log',
        sa.Column('id', sa.Integer(), primary_key=True),
        sa.Column('event_type', sa.String(), nullable=False),
        sa.Column('message', sa.String(), nullable=False),
        sa.Column('timestamp', sa.DateTime(), default=datetime.datetime.utcnow),
    )


def downgrade():
    op.drop_table('audit_log')
